using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;

namespace OneAzureFunction {
	public static class Function1 {
		[FunctionName("Function1")]
		public static IActionResult Run(
			[HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req, TraceWriter log) {
			log.Info("C# HTTP trigger function processed a request.");

			var name = (String)req.Query["name"];

			var requestBody = new StreamReader(req.Body).ReadToEnd();
			dynamic data = JsonConvert.DeserializeObject(requestBody);
			name = name ?? data?.name;

			return name != null
				? (ActionResult)new OkObjectResult($"Hello, {name}")
				: new BadRequestObjectResult("Please pass a name on the query string or in the request body");
		}
	}
}
